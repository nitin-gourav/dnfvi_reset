customer_information = type("customer_information", (object,), dict(
    customer_name="GK_test",
))
customer_information = customer_information()

bp_server_common = type("bp_server_common", (object,), dict(
    bp_host="10.180.77.65",
    bp_user="admin",
    bp_passwd="adminpw",
    flavor_id="PA_SPK_VYOS_FLAVOR",
    level_id="PA_SPK_VYOS_FLAVOR_LEVEL",
    ftp_server="10.180.77.95:22",
    ftp_server_path="/data/sftp/VNF",
    ftp_server_user="ftpuser",
    ftp_server_passwd="ftppass",
    vnfd_dest_path="/CustomerDesigns/vnf_designs/",
    nsd_dest_path="/CustomerDesigns/nsd_designs/",
    nsd_designs_path="/CustomerDesigns/nsd_designs/",
    nsd_file="/CustomerDesigns/nsd_designs/nsdd_pa_spk_vyos.json",
    operation_inputs_file="/CustomerDesigns/resources/instantiationData.json",
    resources_dir="/CustomerDesigns/resources/",
    #	device_info_path = "CustomerDesigns/resources",
    #	nfvi_device_info = "CustomerDesigns/resources/dnfvi.json",
    dnfvi_device1=[
        {"dnfvi_label": "dnfvi-vim1"},
        {"dnfvi_device_ip": "10.180.47.15"},
        {"dnfvi_user": "user"},
        {"dnfvi_password": "ciena123"}
    ],
    dnfvi_device2=[
        {"dnfvi_label": "dnfvi-vim2"},
        {"dnfvi_device_ip": "10.180.47.9"},
        {"dnfvi_user": "user"},
        {"dnfvi_password": "ciena123"}
    ],
    operations=[
        {"arg1": "onboard"},
        #		{"arg1": "discover_device"},
        #		{"arg1": "instantiate"}
    ]
))
bp_server_common = bp_server_common()
