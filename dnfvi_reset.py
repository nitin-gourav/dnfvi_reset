import socket
from netmiko import ConnectHandler, NetmikoTimeoutException
from default_setup import bp_server_common


class dnfvi_reset:
    def parse_sf_file_data(raw_data):
        sf = []
        file = []
        raw = []
        sf_loc = 0
        file_loc = 0
        flag = 0
        lines = raw_data.split("\n")
        for line in lines:
            if "| " in line:
                raw.append(line.split('|')[1].strip())
            if "No NFVI files available to show" in line:
                # print('No files found on the device.')
                flag = 1
        sf_loc = raw.index('SF Name')
        if not flag:
            file_loc = raw.index('File Name')
            sf = raw[sf_loc + 1:file_loc].copy()
            file = raw[file_loc + 1:].copy()
        else:
            sf = raw[sf_loc + 1:].copy()
        return sf, file

    def parse_classifier_data(raw_data):
        classifier_name = []
        lines = raw_data.split("\n")
        for line in lines:
            if "| " in line:
                classifier_name.append(line.split('|')[1].strip())
        del classifier_name[0]
        return classifier_name

    def parse_sff_data(raw_data):
        sff_name = []
        lines = raw_data.split("\n")
        for line in lines:
            if "| " in line:
                sff_name.append(line.split('|')[1].strip())
        del sff_name[0]
        return sff_name

    device = []
    device1 = {'device_type': 'ciena_saos',
                 'host': bp_server_common.dnfvi_device1[1]["dnfvi_device_ip"],
                 'username': bp_server_common.dnfvi_device1[2]["dnfvi_user"],
                 'password': bp_server_common.dnfvi_device1[3]["dnfvi_password"],
                 'port': 830
                 }

    device2 = {'device_type': 'ciena_saos',
                 'host': bp_server_common.dnfvi_device2[1]["dnfvi_device_ip"],
                 'username': bp_server_common.dnfvi_device2[2]["dnfvi_user"],
                 'password': bp_server_common.dnfvi_device2[3]["dnfvi_password"],
                 'port': 830
                 }
    device.append(device1)
    device.append(device2)
    for d in device:
        command_config = []
        try:
            net_connect = ConnectHandler(**d)
            print('+-------Connecting to device:--------+')
            sf_file = net_connect.send_command('show nfvi')
            sf_name, file_name = parse_sf_file_data(sf_file)
            if len(sf_name) > 0 and sf_name[0] != '':
                for x in sf_name:
                    cmd = 'no sfs sf ' + x
                    command_config.append(cmd)
            else:
                print('No sf found on the device.')

            if len(file_name) > 0 and file_name[0] != '':
                for x in file_name:
                    cmd = 'no files file ' + x
                    command_config.append(cmd)
            else:
                print('No file found on the device.')

            sffs = net_connect.send_command('show sffs')
            sff_name = parse_sff_data(sffs)
            if len(sff_name) > 0 and sff_name[0] != '':
                for x in sff_name:
                    cmd = 'no sffs sff ' + x
                    command_config.append(cmd)
            else:
                print('No sff found on the device.')

            classifier = net_connect.send_command('show classifiers')
            classifier_name = parse_classifier_data(classifier)
            if len(classifier_name) > 0 and classifier_name[0] != '':
                for x in classifier_name:
                    cmd = 'no classifiers classifier ' + x
                    command_config.append(cmd)
            else:
                print('No classifier found on the device.')

            if len(command_config) > 0 and command_config[0] != '':
                command_config.insert(0, 'config')
                command_config.append('exit')
                output = net_connect.send_config_set(command_config)
                # print(output)
                print('Device reset successful.')

        except socket.error as e:
            print('Unable to connect to the device.')
            exit()
        except NetmikoTimeoutException as e:
            print('Connection attempt timed out')
            exit()
        net_connect.disconnect()
        print('+-------Connection terminated--------+')
